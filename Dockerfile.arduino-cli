FROM debian:11.4-slim

# Prepare global dependencies for the rest of the install.
RUN DEBIAN_FRONTEND=noninteractive \
    apt-get update \
    && apt-get install -y build-essential curl python3 \
    && apt-get clean
RUN useradd -ms /bin/bash -d /arduino arduino
USER arduino
WORKDIR /arduino

# Install and configure arduino-cli.
ARG ARDUINO_CLI_VERSION
RUN mkdir ~/bin
ENV PATH /arduino/bin:$PATH
RUN curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/$ARDUINO_CLI_VERSION/install.sh > ~/bin/install-arduino-cli \
    && chmod 755 ~/bin/install-arduino-cli
RUN install-arduino-cli $ARDUINO_CLI_VERSION

RUN arduino-cli config init \
    && arduino-cli config set library.enable_unsafe_install true \
    && arduino-cli config set updater.enable_notification false \
    && arduino-cli config set metrics.enabled false

WORKDIR /arduino/build
ENTRYPOINT ["arduino-cli"]
