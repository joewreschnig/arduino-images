# OCI (Docker) Images for Arduino Development

These images can be used to compile or otherwise verify Arduino programs
in CI/CD configurations or for local development in lieu of installing
the Arduino toolchain.


## Images

There are no `latest` images, only versioned ones. You can choose a
version (which should always be compatible), or a version at a specific
GitLab pipeline (which should be immutable).

- `registry.gitlab.com/joewreschnig/arduino-images/arduino-cli` -
  contains [Arduino CLI][] on a Debian system; can be used as a base to
  install specific cores or libraries

- `registry.gitlab.com/joewreschnig/arduino-images/rp2040` - contains
  the base image plus the [rp2040][] core preinstalled

- `registry.gitlab.com/joewreschnig/arduino-images/thumby` - contains
  the rp2040 core plus the [Thumby][] libraries preinstalled


## Usage

(These examples use [Podman][] rather than Docker, but the images also
work in Docker. Some options regarding user/volume mapping may be more
complex when using the Docker tools.)

The default entrypoint is `arduino-cli`; an interactive shell can be
acquired via e.g. `podman run -it --entrypoint /bin/bash …` to override
this.

The images run as an `arduino` user with UID 1000, not root. You will
need to map your local user to that. If you’re using [Podman][], use
`--userns=keep-id`; no root access is required in or out of the
container.

The default work directory is `/arduino/build`, which you should mount
your project inside. E.g. to compile a sketch in your current directory
and get the build artifacts back via the `output` subdirectory:

```
podman run --userns=keep-id -v $PWD:/arduino/build/${PWD##*/}:Z \
    -w /arduino/build/${PWD##*/} \
    registry.gitlab.com/joewreschnig/arduino-images/rp2040:2.1.1 \
    compile --output-dir output
```

(`${PWD##*/}` is a shell variable replacement for only the last element
of the working directory.)


[Arduino CLI]: https://arduino.github.io/arduino-cli/latest/
[podman]: https://podman.io/
[rp2040]: https://github.com/earlephilhower/arduino-pico
[Thumby]: https://thumby.us/CCPP/Environment-Setup/
